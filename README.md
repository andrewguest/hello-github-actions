## Welcome to "Hello World" with GitHub Actions

This course will walk you through writing your first action and using it with a workflow file.

![GitHub Workflow Status (branch)](https://img.shields.io/github/workflow/status/andrewguest/hello-github-actions/Python%20workflow/master?style=for-the-badge)

**Ready to get started? Navigate to the first issue.**